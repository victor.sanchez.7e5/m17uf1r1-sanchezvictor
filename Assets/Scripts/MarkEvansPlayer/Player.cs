using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    private Animator playerAnimator;


    public static bool youDied;

    private void Start()
    {
        youDied = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.collider.tag == "fireball")
        {
            playerAnimator.SetBool("die", true);
            Debug.Log("You Died !");
            StartCoroutine(Muriendo());

        }

    }

    private IEnumerator Muriendo()
    {
        yield return new WaitForSeconds(1f);
        youDied = true;
    }
}
