using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Patrol : MonoBehaviour
{

    private bool isFacingRight = true;

    [SerializeField] private Rigidbody2D rb;


    [SerializeField]
    private Animator animator;


    public float velocidad = 7.5f;
    public float distancia = 10f;

    private int direccion = 1; // 1 representa la direcci�n hacia la derecha, -1 representa la direcci�n hacia la izquierda
    private float posicionInicial;

    void Start()
    {
        posicionInicial = transform.position.x;
    }

    // Update is called once per frame
    void Update()
    {

            float movimiento = velocidad * Time.deltaTime * direccion;
            transform.Translate(new Vector3(movimiento, 0, 0));

            
            // Comprobar si ha llegado a la distancia l�mite
            if (Mathf.Abs(transform.position.x - posicionInicial) >= distancia)
            {
                    // Cambiar la direcci�n

                    isFacingRight = false;
                    direccion *= -1;
            }

            animator.SetFloat("InputhoritzontalAxis", 1f);

        Flip();

    }

    //private void FixedUpdate()
    //{
    //    rb.velocity = new Vector2(horizontal * velocitat, rb.velocity.y);
    //}

    //private void Flip()
    //{
    //    if (isFacingRight) this.gameObject.GetComponent<SpriteRenderer>().flipX = true; 
    //    else this.gameObject.GetComponent<SpriteRenderer>().flipX = false;
    //}

    private void Flip()
    {
        if (isFacingRight && direccion < 0f || !isFacingRight && direccion > 0f)
        {
            isFacingRight = !isFacingRight;
            Vector3 localScale = transform.localScale;
            localScale.x *= -1f;
            transform.localScale = localScale;
        }
        

    }
}
