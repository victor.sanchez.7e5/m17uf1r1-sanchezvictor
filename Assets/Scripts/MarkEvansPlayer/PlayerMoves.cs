using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoves : MonoBehaviour
{
    [SerializeField]
    private int frameRate;

    //X movement
    private float horizontal;

    private float velocitat = 10f;    
    private float jumpPower = 10f;
    private bool isFacingRight = true;

    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private Transform groundCheck;
    [SerializeField] private LayerMask groundLayer;

    [SerializeField]
    private Animator animator;

    
    // Update is called once per frame
    void Update()
    {


        horizontal = Input.GetAxisRaw("Horizontal");

        if (Input.GetAxisRaw("Horizontal") > 0 ||  Input.GetAxisRaw("Horizontal") == -1)
        {
            //Debug.Log("Me estoy Moviendo"); 
            animator.SetFloat("InputhoritzontalAxis", 1f); 
        }else
        {
            animator.SetFloat("InputhoritzontalAxis", -1f);     // Puc millorar la funcionalitat ja que triga com un segon a tornar a IDLE
                                                                // pero fa el moviment basic
        }


        if (Input.GetButtonDown("Jump") && isGrounded())
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpPower);
        }

        if (Input.GetButtonUp("Jump") && rb.velocity.y > 0f)
        {
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.5f);
        }


        Flip();

        GrowOnBPressed();

    }


    public void GrowOnBPressed()
    {
        if (Input.GetKeyDown("b") && isMaxSize == false)
        {

            for (int i = 1000; i > 0; i--)    //refredament de l'habilitat
            {
                //transform.localScale = startScale;    
            }
            StartCoroutine(Grow());
        }
    }

    private void FixedUpdate()
    {
        rb.velocity = new Vector2(horizontal * velocitat, rb.velocity.y);
    }

    private bool isGrounded()
    {
        return Physics2D.OverlapCircle(groundCheck.position, 0.2f, groundLayer);
    }

    private void Flip()
    {
        if (isFacingRight && horizontal < 0f || !isFacingRight && horizontal > 0f)
        {
            isFacingRight = !isFacingRight;
            Vector3 localScale = transform.localScale;
            localScale.x *= -1f;
            transform.localScale = localScale;
        }
    }


    public float timer = 0f;
    public float growTime = 6f;
    public float maxSize = 2f;
    public bool isMaxSize = false;
    /// <summary>
    /// Method to Grow the Sprite Scale
    /// </summary>
    /// <returns></returns>
    public IEnumerator Grow()
    {
        Vector2 startScale = transform.localScale;
        Vector2 maxScale = new Vector2(maxSize, maxSize);
        do
        {
            transform.localScale = Vector3.Lerp(startScale, maxScale, timer / growTime);
            timer += Time.deltaTime;

            yield return null;
        } while (timer < growTime);
        isMaxSize = true;

    }
}
