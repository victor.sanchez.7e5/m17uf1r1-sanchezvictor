using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class ChangeSceneButton : MonoBehaviour
{
    public static bool playerGame = false;
    public static bool fireballGame = false; 

    public void GameScenePlayer()
    {
        SceneManager.LoadScene("MainGame");
        playerGame = true;
        fireballGame = false; 
    }

    public void GameSceneFireBall()
    {
        SceneManager.LoadScene("Mode2MainGame");
        fireballGame = true;
        playerGame = false;
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene("StartGameScene");
    }

    public void ExitProgram()
    {
        Application.Quit(); 
    }

}

