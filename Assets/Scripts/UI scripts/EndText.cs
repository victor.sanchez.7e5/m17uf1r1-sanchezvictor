using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndText : MonoBehaviour
{

    private GameObject WinOrLosingCondition;

    private void Start()
    {
        WinOrLosingCondition = GameObject.Find("EndConditionTxt");
    }

    void Update()
    {
        
        if (Timer.timeOver && ChangeSceneButton.playerGame || Player.youDied && ChangeSceneButton.fireballGame)
        {
            WinOrLosingCondition.GetComponent<Text>().color = Color.green;
            WinOrLosingCondition.GetComponent<Text>().text = " YOU WIN THIS GAME";
        }
        else if (Player.youDied == true && ChangeSceneButton.playerGame || OutFieldDrinking.youDied == true || ChangeSceneButton.fireballGame && Timer.timeOver)
        {
            WinOrLosingCondition.GetComponent<Text>().color = Color.red;
            WinOrLosingCondition.GetComponent<Text>().text = "GAME OVER";

        }
    }
}
