using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public float timerValue = 90;
    public Text timerText;
    public static bool timeOver;


    private void Start()
    {
        timeOver = false; 
    }
    void Update()
    {
        if (timerValue < 0) timeOver = true;

        if (timerValue > 0)
        {
            timerValue -= Time.deltaTime;
        }
        else
        {
            timerValue += 0;
        }
        DisplayTime(timerValue);

        
    }

    void DisplayTime (float timeToDisplay)
    {
        if (timeToDisplay < 0)
        {
            timeToDisplay = 0; 
        }

        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);
        timerText.text = string.Format("{0:00}: {1:00}", minutes, seconds); 
                
    }
}
