using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{
    private GameObject player;
    private GameObject fireball;
    private GameObject ball;
    private GameObject manodedios;
    private GameObject superpuñoinvencible;
    private GameObject TimerText;


    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("markevans_1");
        fireball = GameObject.Find("Fireball");
        ball = GameObject.Find("ball");
        manodedios = GameObject.Find("manodedios");
        superpuñoinvencible = GameObject.Find("superpuñoinvencible");

        TimerText = GameObject.Find("TimerText");


        //fireball.SetActive(false);
        manodedios.SetActive(false);
        superpuñoinvencible.SetActive(false);

    }

    void Update()
    {
        if (Timer.timeOver == true)
        {
            
            SceneManager.LoadScene("GameEndScene");

        }

        else if (Player.youDied == true || OutFieldDrinking.youDied == true)
        {

            SceneManager.LoadScene("GameEndScene");

        }

    }
}
