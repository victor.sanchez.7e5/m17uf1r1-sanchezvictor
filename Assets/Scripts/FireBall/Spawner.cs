using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using static UnityEngine.RuleTile.TilingRuleOutput;

public class Spawner : MonoBehaviour
{

    [SerializeField]
    public GameObject fireball;

    float next_spawn_time;

    //Fireball Timer
    private float timer;
    public float initialSpawnTime = 2.0f;
    public float spawnTimeIncrease = 0.5f;
    private float currentSpawnTime;

    // Start is called before the first frame update
    void Start()
    {
        //fireball = GameObject.Find("Fireball");
        //fireball.SetActive(true);

        next_spawn_time = Time.time + 1.5f;


        //Fireball Timer
        currentSpawnTime = initialSpawnTime;
        timer = currentSpawnTime;
    }

    // Update is called once per frame
    private void Update()
    {
        if (ChangeSceneButton.playerGame)
        { 
            if (Time.time > next_spawn_time)
            {
                Spawn();
                next_spawn_time += 5.0f;
                Debug.Log("PLAYER MODE");
            }
        }

        //Fireball Mode Spawner

        //if (ChangeSceneButton.fireballGame)
        //{
        //    timer -= Time.deltaTime;

        //    if (timer <= 0.0f)
        //    {
        //        fireball.GetComponent<Rigidbody2D>().gravityScale = 10;     

        //        SpawnFireballPlayer();

        //        Debug.Log("FIREBALL MODE");
        //        currentSpawnTime += spawnTimeIncrease;
        //        timer = currentSpawnTime;
        //    }
        //}
        
    }

    private void Spawn()
    {
            Instantiate(fireball, new Vector3(Random.Range(-5.9f, 5.8f), Random.Range(5.3F, 15F), -0.13f), transform.rotation);
    }

    //private void SpawnFireballPlayer()
    //{
    //        Instantiate(fireball, new Vector3(Random.Range(-5.9f, 5.8f), Random.Range(5.3F, 15F), -0.13f), transform.rotation);
    //}
}
