using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballController : MonoBehaviour
{
    public GameObject fireballPrefab;
    public float groundHeight = 5.0f;

    public float cooldownTime = 3.0f;

    private float cooldownTimer = 0.0f;
    private void Update()
    {
        cooldownTimer -= Time.deltaTime;

        if (cooldownTimer <= 0.0f && Input.GetMouseButtonDown(0))
        {
            Vector3 mousePosition = Input.mousePosition;
            mousePosition.z = 9.87f; 
            Vector3 spawnPosition = Camera.main.ScreenToWorldPoint(mousePosition);
            spawnPosition.y = groundHeight;

            SpawnFireball(spawnPosition);

            cooldownTimer = cooldownTime;
        }
    }

    private void SpawnFireball(Vector3 spawnPosition)
    {
        Instantiate(fireballPrefab, spawnPosition, Quaternion.identity);
    }

}
