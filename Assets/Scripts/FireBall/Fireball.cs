using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
    [SerializeField]
    private Animator fireballAnimator;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player" || collision.collider.tag == "floor")
        {
            Debug.Log("Explota");

            fireballAnimator.SetBool("fireballCollision", true);

            StartCoroutine(Explotando()); 
        }

    }

    private IEnumerator Explotando()
    {
        yield return new WaitForSeconds(0.5f);

        Destroy(this.gameObject);
    }
}
